require 'gollum/app'

Io::Application.routes.draw do
  root :to => "home#index"

  resources :trainings, :only => [:index] do
    resources :sessions
    get 'schedule'
  end

  namespace :admin do
    resources :trainings, only: [:index, :update, :create] do
      member do
        get 'schedule', :as => :schedule
        get 'export.:format' => "trainings#export", :as => :export
      end
    end

    resources :users, :only => [:index, :show, :edit] do
      member do
        put 'update_role'
      end
    end

    get 'wiki/pages' => 'wiki#pages'
  end

  get '/apache/vhost' => 'apache#vhost'

  get '/auth/:provider/callback' => 'login#create'
  get '/signin' => 'login#new', :as => :signin
  get '/signout' => 'login#destroy', :as => :signout
  post '/auth/failure' => 'login#failure'

  mount Precious::MyApp, at: 'wiki'
end
