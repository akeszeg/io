set :stages, %w(production staging)
set :default_stage, "staging"

require 'capistrano/ext/multistage'

# set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,"")
set :rvm_ruby_string, "ruby-1.9.3-p385"

set :application, "io"
set :user, "akeszeg"
set :use_sudo, false
set :scm, :git
set :keep_releases, 5

# set :rvm_ruby_string, ENV['GEM_HOME'].gsub(/.*\//,"")
set :rvm_type, :system
#set :bundle_without,  [:development]

server "trainingsystem", :app, :db, :web, :primary => true

before 'deploy:setup', 'rvm:create_gemset'
after 'deploy:update_code', 'deploy:symlink_config'
after 'deploy:setup', 'deploy:create_shared_files'

# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

load 'deploy/assets'

#If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  desc "Symlinks the database.yml"
  task :symlink_config, :roles => :app do
    run "ln -nfs #{deploy_to}/shared/config/database.yml #{release_path}/config/database.yml"
    run "cp #{deploy_to}/shared/config/env #{release_path}/.env"
    run "ln -nsf #{deploy_to}/shared/wiki #{release_path}/db/wiki.git"
  end

  task :create_shared_files, :roles => :app do
    run "mkdir #{deploy_to}/shared/wiki/"
    run "mkdir #{deploy_to}/shared/config/"
    run "git init --bare #{deploy_to}/shared/wiki/"
    run "touch #{deploy_to}/shared/config/database.yml"
    run "touch #{deploy_to}/shared/config/env"
  end
end

require 'bundler/capistrano'
require "rvm/capistrano"

require 'whenever/capistrano'
set :whenever_command, "bundle exec whenever"
