set :repository, 'git@bitbucket.org:akeszeg/io.git'
set :branch, "develop"
set :deploy_to, "/var/sites/#{application}/staging/"
set :whenever_environment, 'staging'
set :rails_env, "staging"
