set :repository, 'git@bitbucket.org:akeszeg/io.git'
set :branch, "master"
set :deploy_to, "/var/sites/#{application}/production"
set :whenever_environment, 'production'
