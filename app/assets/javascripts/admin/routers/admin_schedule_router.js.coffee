class Io.Admin.Routers.SchedulesRouter extends Backbone.Router
  initialize: (options) ->
    @training = options.training
    @calendar = new Io.Models.Calendar(@training)
    @sessions = new Io.Collections.SessionsCollection(options.sessions)
    @trainers = new Io.Collections.UsersCollection(options.users)

    window.sessions = @sessions
    window.trainers = @trainers

    @indexView = new Io.Admin.Views.Schedules.IndexView(
      sessions: @sessions
      training: @training
      calendar: @calendar
      trainers: @trainers
    )

  routes:
    "": "index"

  index: ->
    @indexView.render()

