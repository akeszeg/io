Io.Admin.Views.Wiki ||= {}

class Io.Admin.Views.Wiki.Pages extends Backbone.View
  template: JST["admin/templates/wiki/file_view"]

  events:
    'click .save': 'save'
    'click .cancel': 'cancel'

  render: =>
    @$el.empty()
    @$el.append(@template)
    @jstree = @$('.pages').jstree
      json_data:
        ajax:
          url: Routes.admin_wiki_pages_path()
      themes:
        icons: false
      plugins: ['themes', 'ui', 'json_data']
    return this

  show: =>
    $(document.body).append(@render().el)

  save: =>
    val = []
    _.each @jstree.jstree('get_selected'), (e) =>
      val.push($(e).attr('id')) if $(e).attr('id')?

    @remove()
    @trigger('save', selectedItemIds: val)

  cancel: =>
    @remove()
