Io.Admin.Views.Trainers ||= {}

class Io.Admin.Views.Trainers.AssignedResponsibles extends Backbone.View
  initialize: =>
    @collection.on('add remove change', @render, this)

  render: ->
    @$el.empty()
    _.each @collection.models, (user) =>
      view = new Io.Views.Trainers.ShowView(collection: @collection, model: user)
      @$el.append(view.render().el)


