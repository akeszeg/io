Io.Admin.Views.Schedules ||= {}

class Io.Admin.Views.Schedules.ShowView extends Backbone.View
  template: JST["admin/templates/schedules/day"]
  tagName: 'tr'

  events:
    # Don't select the element when the link is clicked
    'click .wiki_link': 'trapClick'

  initialize:(options) ->
    Backbone.Courier.add(this)

    @trainers = @options.trainers
    @calendar = @options.calendar

    @listenTo(@model, 'add remove change', @render, this)
    @listenTo(@model.get('responsibles'), 'add remove', @render, this)
    @listenTo(@model.get('responsibles'), 'add remove', @updateResponsibles, this)

    @listenTo(@model, 'selected', @showEditView, this)
    @listenTo(@model, 'deselected', @hideEditView, this)

  trapClick: (e) =>
    e.stopPropagation()

  showEditView: =>
    @editView = new Io.Admin.Views.Schedules.EditView(model: @model) unless @editView?
    @editView.setElement(@$('.edit_form')).render()

  hideEditView: =>
    if @editView?
      @editView.close()
      @editView.remove()
      @editView = null
    @render()

  date: ->
    @calendar.nextDate(@model.get('position'))

  render: ->
    @$el.empty()
    @$el.removeAttr('class')

    @$el.addClass('day')
    @$el.addClass(@trClass())

    @$el.append @template(
      session: @model
      dayOfWeek: @formattedDayOfWeek()
      date: @formattedDate()
    )

    @refreshEvents()

    @usersList = new Io.Admin.Views.Trainers.AssignedResponsibles(collection: @model.get('responsibles'))
    @usersList.setElement(@$('.responsibles')).render()

    if @editView?
      @editView.updateModel()
      @editView.setElement(@$('.edit_form')).render()

    return this

  refreshEvents: ->
    @$el.droppable(
      accept: '.responsible'
      hoverClass: 'drop_hover'
      drop: @handleDropEvent
    )

  handleDropEvent: (event, ui) =>
    id = ui.draggable.data('id')
    user = @trainers.findWhere({id: id})
    @model.addResponsible(user)

  updateResponsibles: =>
    @eventAggregator.trigger('session:save', @model)

  trClass: ->
    classes = []
    if (@model.isFree())
      classes.push('free')
    if (@model.hasResponsibles())
      classes.push('assigned')
    if moment(@date()).week() % 2
      classes.push('even')
    else
      classes.push('odd')

    classes.join(' ')

  formattedDayOfWeek: ->
    moment(@date()).format('dd')

  formattedDate: ->
    moment(@date()).format('MMM Do')

