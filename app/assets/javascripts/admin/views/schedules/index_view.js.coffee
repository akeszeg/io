Io.Admin.Views.Schedules ||= {}

class Io.Admin.Views.Schedules.IndexView extends Backbone.View
  template: JST["admin/templates/schedules/index"]
  el: '#schedules'

  onMessages:
    # from @view
    'add': 'collectionViewAddModel'
    'sortStop': 'collectionViewSortStop'

  events: ->
    "click .js_add_button": "addButtonClicked"
    "click .js_delete_button": "removeButtonClicked"

  initialize: ->
    Backbone.Courier.add(this)

    @sessions = @options.sessions
    @training = @options.training
    @trainers = @options.trainers

    @messagesView = new Io.Views.Messages.Alert()

    @listenTo(@eventAggregator, 'session:deselected', @collectionViewSelectionClear, this)
    @listenTo(@eventAggregator, 'session:save', @saveSession, this)
    @listenTo(@sessions, 'reorder remove', @sessionsComputePosition, this)

  #
  # Model CRUD
  #
  addSession: (session) =>
    session.save(null,
      success: (session) =>
        @selectAndShowSession(session)
      error: (session, jqXHR) =>
        @eventAggregator.trigger('alert', 'Failed to create resource.')
        session.set({errors: $.parseJSON(jqXHR.responseText)})
      )

  deleteSession: (session) =>
    previousIndex = @sessions.indexOf(session)

    session.destroy(
      success: =>
        @sessionDeleted(session, previousIndex)
    )

  saveSession: (session) =>
    session.save(null,
      error: (session, jqXHR) =>
        @eventAggregator.trigger('alert', 'Failed to save changes!')
    )

  #
  # View CRUD
  #
  addButtonClicked: =>
    index = @computeAddIndex()
    model = new Io.Models.Session({position: index, training_id: @training.id})

    @sessions.add(model, at: index)
    @addSession(model)
    @sessionsComputePosition()

  computeAddIndex: =>
    if @hasSelection()
      @sessions.indexOf(@view.getSelectedModels()[0]) + 1
    else
      @sessions.size()

  hasSelection: =>
    @view.getSelectedModels().length > 0

  removeButtonClicked: =>
    models = @view.getSelectedModels()
    for model in models
      @deleteSession(model)

  #
  # View events
  #
  selectAndShowSession: (session) =>
    view = @view.viewManager.findByModel(session)
    @view.setSelectedModels([session])

    $.scrollTo($(view.el), offset: -100)

  sessionDeleted: (model, previousIndex) =>
    newModel = @sessions.at(previousIndex - 1)
    @view.collection.remove(model)
    @selectAndShowSession(newModel) if newModel

  collectionViewAddModel: (event) =>
    @highlightModel(event.data.model)

  collectionViewSortStop: (event) =>
    @highlightModel(event.data.modelBeingSorted)

  highlightModel: (model) ->
    sessionView = @view.viewManager.findByModel(model)
    sessionView.$el.effect("highlight", {}, 400)

  collectionViewSelectionChanged: (newSelectedModels, oldSelectedModels) ->
    for model in newSelectedModels
      model.trigger('selected')

    for model in oldSelectedModels
      model.trigger('deselected')

  updateDependentControls: =>
    @$('.js_delete_button').prop('disabled', !(@view.getSelectedModels().length > 0))

  collectionViewSelectionClear: (model) =>
    @view.setSelectedModels([])

  sessionsComputePosition: =>
    for session in @sessions.models
      newIndex = @sessions.indexOf(session)
      if session.get('position') != newIndex
        session.set("position", newIndex)
        @saveSession(session)
  #
  # Render
  #
  render: ->
    $(@el).append(@template)

    @view = new Backbone.CollectionView({
      el: $(@el).find('table'),
      selectable: true,
      sortable: true,
      collection: @sessions,
      modelView: Io.Admin.Views.Schedules.ShowView,
      modelViewOptions: {
        calendar: @options.calendar
        trainers: @trainers
        },
      sortableOptions: {
          placeholder: "ui-state-highlight"
        }
      })

    @listenTo(@view, 'selectionChanged', @collectionViewSelectionChanged, this)
    @listenTo(@view, 'updateDependentControls', @updateDependentControls, this)
    @view.render()

    @trainersView = new Io.Views.Trainers.ListView(collection: @trainers)
    @trainersView.render()

    window.view = @view

    return this
