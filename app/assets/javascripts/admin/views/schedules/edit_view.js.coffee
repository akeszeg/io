Io.Admin.Views.Schedules ||= {}

class Io.Admin.Views.Schedules.EditView extends Backbone.View
  template: JST["admin/templates/schedules/edit"]

  events:
    'click .close_button': 'closeButton'
    'click .select_wiki_page': 'selectWikiPages'
    'click .clear_button': 'clearWikiPages'
    'keyup input[name="title"]': 'keypressHandler'

  keypressHandler: (e) =>
    # escape
    if e.which == 27
      @escape()

    # enter
    if e.which == 13
      @closeButton(e)

  escape: =>
    @eventAggregator.trigger('session:deselected', @model)

  updateModel: =>
    @model.set(
      title: $(@el).find('[name="title"]').val()
      is_free: $(@el).find('[name="is_free"]').is(":checked")
    )

  saveModel: =>
    changed = @model.hasChanged()
    @updateModel()

    changed = changed || @model.hasChanged()

    @eventAggregator.trigger('session:save', @model) if changed

  closeButton: (e) =>
    e.preventDefault()
    @eventAggregator.trigger('session:deselected', @model)
    @saveModel()

  close: =>
    @saveModel()

  selectWikiPages: =>
    popup = new Io.Admin.Views.Wiki.Pages()
    @listenTo(popup, 'save', @setWikiPages)
    popup.show()

  clearWikiPages: (e) =>
    e.preventDefault()
    @model.set('wiki_pages', [])

  setWikiPages: (e) =>
    @model.set('wiki_pages', e.selectedItemIds)

  focus: =>
    setTimeout =>
      $('input[name="title"]').focus()

  render: ->
    @$el.empty()
    @$el.append(@template(session: @model))
    @focus()

    return this
