Io.Views.ApacheVhosts ||= {}

class Io.Views.ApacheVhosts.GenerateView extends Backbone.View
  template: JST["io/templates/apache_vhosts/vhost"]

  initialize: (options) ->
    @value = options.value

  domainName: ->
    @value || "example.com"

  render: ->
    @$el.html @template(
      domainName: @domainName()
      username: @options.username
    )

    return this
