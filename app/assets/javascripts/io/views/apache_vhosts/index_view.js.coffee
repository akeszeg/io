Io.Views.ApacheVhosts ||= {}

class Io.Views.ApacheVhosts.IndexView extends Backbone.View
  template: JST["io/templates/apache_vhosts/index"]

  el: '#vhost'

  events:
    'keyup .search-query': 'generateCode'

  input: ->
    @$('.search-query')

  generateCode: (e) =>
    @view = new Io.Views.ApacheVhosts.GenerateView(value: @input().val(), username: @$el.data('username'))
    @view.setElement(@$('.output')).render()

  render: ->
    @$el.html(@template())
    return this
