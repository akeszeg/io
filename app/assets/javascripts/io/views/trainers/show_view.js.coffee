Io.Views.Trainers ||= {}

class Io.Views.Trainers.ShowView extends Backbone.View
  template: JST["io/templates/trainers/show"]

  tagName: 'span'

  events:
    'click .remove': 'removeUser'
    'click': 'click'

  render: ->
    @$el.empty()
    @$el.html(@template(model: @model))

    return this

  removeUser: =>
    @collection.remove(@model)

  # stop the list view form starting a sort
  click: (e) =>
    e.stopPropagation()

