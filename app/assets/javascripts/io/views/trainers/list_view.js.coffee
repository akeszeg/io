Io.Views.Trainers ||= {}

class Io.Views.Trainers.ListView extends Backbone.View
  template: JST["io/templates/trainers/list"]

  el: '.right_block'

  render: ->
    @$el.empty()
    @$el.append(@template(collection: @collection))
    @refreshEvents()

    return this

  refreshEvents: ->
    @$('.draggable').draggable(
      helper: 'clone'
    )

    new Io.Views.ScrollView('.schedule_table', '.trainers')
    new Io.Views.ScrollView('.schedule_table', '.controls')
