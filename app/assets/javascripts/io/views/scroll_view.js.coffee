Io.Views ||= {}

class Io.Views.ScrollView
  constructor: (relatedElement, el) ->
    $window = $(window)
    $floatedElement = $(el)
    $relatedElement = $(relatedElement)

    elementOffset = $relatedElement.offset().top

    $window.scroll =>
      navbarHeight = $('.navbar-fixed-top').height()
      offset = elementOffset - navbarHeight
      scrollTop = $window.scrollTop()

      if offset < scrollTop
        $floatedElement.addClass('fixed')
        if @isScrolledIntoView($('.navbar-fixed-top'))
          $floatedElement.css(top: "#{navbarHeight}px")
        else
          $floatedElement.css(top: '0')
      else
        $floatedElement.removeClass('fixed')
        $floatedElement.css(top: '')

  isScrolledIntoView: (elem) ->
    docViewTop = $(window).scrollTop()
    docViewBottom = docViewTop + $(window).height()

    elemTop = elem.offset().top
    elemBottom = elemTop + elem.height()

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop))

