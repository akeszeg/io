Io.Views.Messages ||= {}

class Io.Views.Messages.Alert extends Backbone.View
  template: JST["io/templates/messages/message"]

  el: '#container'

  initialize: ->
    @listenTo(@eventAggregator, 'alert', @render, this)

  render: (message) ->
    @$el.prepend(@template(error: message))



