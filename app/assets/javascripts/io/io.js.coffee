#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events)

window.Io =
  Admin:
    Routers: {}
    Views: {}
  Models: {}
  Collections: {}
  Routers: {}
  Views:
    Apache: {}

