class Io.Routers.ApacheVhostsRouter extends Backbone.Router
  initialize: (options) ->

  routes:
    "": "index"

  index: ->
    @view = new Io.Views.ApacheVhosts.IndexView()
    $("#apache_vhosts").html(@view.render().el)

