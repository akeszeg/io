# Computes the next week day from start.
class Io.Models.Calendar extends Backbone.Model
  initialize: (training) =>
    @memo = []
    @training = training
    @startsAt = new Date(@training.get('starts_at'))

  nextWeekDay: (date) ->
    date.setDate(date.getDate() + 1)
    @dayOrNextWeekDay(date)

  dayOrNextWeekDay: (date) ->
    while date.getDay() == 0 || date.getDay() == 6
      date.setDate(date.getDate() + 1)
    return date

  nextDate: (position) ->
    if position of @memo
      return @memo[position]

    if position == 0
      date = @dayOrNextWeekDay(@startsAt)
    else
      date = @nextWeekDay(new Date(@nextDate(position - 1)))

    @memo[position] = date
    date


