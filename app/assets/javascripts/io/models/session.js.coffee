class Io.Models.Session extends Backbone.AssociatedModel
  relations: [
    type: Backbone.Many
    key: 'responsibles'
    relatedModel: 'Io.Models.User'
    collectionType: 'Io.Collections.UsersCollection'
  ]

  url: ->
    path = '/trainings/' + @get('training_id') + '/sessions'
    path += "/#{@id}" if @id?
    path

  isFree: ->
    @get('is_free') in [true, "true"]

  getTitle: ->
    if @isFree()
      'Liber'
    else
      @get('title')

  isSelected: ->
    @get('selected')

  addResponsible: (user) ->
    @get('responsibles').add(user)

  hasResponsibles: ->
    @get('responsibles').length > 0

  defaults:
    selected: false
    responsibles: []
    wiki_pages: []

class Io.Collections.SessionsCollection extends Backbone.Collection
  model: Io.Models.Session
  url: '/sessions'
