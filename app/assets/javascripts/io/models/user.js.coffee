class Io.Models.User extends Backbone.AssociatedModel
  relations: [
    type: Backbone.Many
    key: 'sessions'
    relatedModel: 'Io.Models.Session'
    collectionType: 'Io.Collections.SessionsCollection'
  ]

  defaults:
    sessions: []
    roles: []

class Io.Collections.UsersCollection extends Backbone.Collection
  model: Io.Models.User
  url: '#'

