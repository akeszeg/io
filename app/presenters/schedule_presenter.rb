class SchedulePresenter
  attr_reader :current_user

  def initialize(current_user)
    @current_user = current_user

  end

  def show_edit_link?
    trainer?
  end

  def show_responsibles?(session)
    trainer? && session.responsibles.any?
  end

  def css_class_for_session(session)
    c = []
    if trainer?
      c << 'today' if session.starts_at == Date.today
      #c << 'assigned' if session.responsibles.include?(user)
    end

    c.join(' ')
  end

  def trainer?
    current_user.can? :manage_schedule, Training
  end
end
