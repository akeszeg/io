class Session < ActiveRecord::Base
  attr_accessible :position, :title, :training_id, :is_free, :wiki_pages

  belongs_to :training
  serialize :wiki_pages

  has_and_belongs_to_many :responsibles, join_table: 'sessions_responsibles', class_name: 'User'

  validates_presence_of :training_id, :position

  def has_wiki_pages?
    wiki_pages.present? && wiki_pages.length > 0
  end
end

