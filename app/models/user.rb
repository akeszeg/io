class User < ActiveRecord::Base
  rolify

  attr_accessible :provider, :uid, :name, :email
  attr_accessible :provider, :uid, :name, :email, :role_ids, :as => :admin

  has_many :sessions

  scope :trainers, -> { with_role('trainer') }

  def accessible_by
    [User.first]
  end

  def author_info
    { name: name, email: email }
  end

  # ============
  # Ability
  # ============

  def ability
    @ability ||= Ability.new(self)
  end

  def username
    email[/(^.*)@/, 1]
  end

  def member_of_staff?
    has_role?(:trainer) || has_role?(:staff)
  end

  delegate :can?, :cannot?, :to => :ability
end
