class Training < ActiveRecord::Base
  attr_accessible :starts_at, :title, :is_active

  has_many :sessions

  scope :active, -> { where(is_active: true) }

  validates_presence_of :starts_at
end
