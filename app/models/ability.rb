class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin
      can :manage, :all
    end

    can :change_role, User if user.has_role?(:admin)

    if user.member_of_staff?
      can :manage, Precious::MyApp
      can :manage_schedule, Training
      can :manage, Training
      can :manage, Session
      can :manage, Gollum::Wiki
    end
  end
end
