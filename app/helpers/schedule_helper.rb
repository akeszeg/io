module ScheduleHelper
  def format_user_list(users)
    users.collect { |user| user.name }.join(', ')
  end
end
