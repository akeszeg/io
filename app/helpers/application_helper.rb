module ApplicationHelper
  def wiki_page_url(page)
    "#{root_url}wiki/#{page}"
  end
end
