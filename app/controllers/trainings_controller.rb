class TrainingsController < AuthenticatedController
  def schedule
    @training = ::Training.find(params[:training_id])
    @presenter = SchedulePresenter.new(current_user)

    schedule = Io::Session::Schedule.new(@training, current_user)
    @sessions = schedule.sessions
  end
end
