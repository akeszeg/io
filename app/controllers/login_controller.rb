class LoginController < ApplicationController
  before_filter :find_or_create_user, :only => [:create]

  def new
    redirect_to '/auth/google_oauth2'
  end

  def create
    session[:user_id] = @user.id
    @user.add_role :admin if User.count == 1 # make the first user an admin
    if @user.email.blank?
      redirect_to edit_user_path(user), :alert => "Please enter your email address."
    else
      redirect_to root_url, :notice => 'Signed in!'
    end

  end

  def destroy
    reset_session
    redirect_to root_url, :notice => 'Signed out!'
  end

  def failure
    redirect_to root_url, :alert => "Authentication error: #{params[:message].humanize}"
  end

  private

  def find_or_create_user
    auth = request.env["omniauth.auth"]
    @user = User.where(:provider => auth['provider'], :uid => auth['uid'].to_s).first

    unless @user
      register = Io::User::Register.new(request.env["omniauth.auth"])
      if register.perform
        @user = register.user
      else
        redirect_to root_url, :alert => "Authentication failure: #{register.errors[:base].first}"
      end
    end
  end
end
