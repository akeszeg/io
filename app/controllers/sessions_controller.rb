class SessionsController < AuthenticatedController
  load_and_authorize_resource :training
  load_and_authorize_resource :session, through: :training

  # GET /sessions
  # GET /sessions.json
  def index
    schedule = Io::Session::Schedule.new(@training, current_user)

    respond_to do |format|
      format.json { render json: schedule.sessions }
    end
  end

  # POST /sessions.json
  def create
    @command = Io::Session::Update.new(params[:session].merge(session: Session.new(training_id: params[:session][:training_id])))

    respond_to do |format|
      if @command.perform
        format.json { render json: @command.session, status: :created }
        format.json { head :no_content }
      else
        format.json { render json: @command.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sessions/1.json
  def update
    respond_to do |format|
      attr = params[:session].merge(
        responsible_ids: params[:responsibles].present? ? params[:responsibles].collect { |user| user[:id] } : [],
        session: @session
      )

      @command = Io::Session::Update.new(attr)
      if @command.perform
        format.json { head :no_content }
      else
        format.json { render json: @command.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sessions/1.json
  def destroy
    @session.destroy

    respond_to do |format|
      format.json { head :no_content }
    end
  end
end
