class Admin::WikiController < AdminController
  def pages
    @wiki = Gollum::Wiki.new(Rails.root.join(ENV['WIKI_PATH']), {})
    authorize! :manage, @wiki

    pages = Io::Wiki::Pages.new(@wiki)
    render json: pages.to_json
  end

end
