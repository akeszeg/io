class Admin::UsersController < AdminController
  load_and_authorize_resource

  def index
    @users = User.all
  end

  def update_role
    @user = User.find(params[:id])
    authorize! :change_role, @user

    if @user.update_attributes(params[:user], as: :admin)
      flash[:success] = "Updated #{@user.name}"
      redirect_to admin_users_path
    else
      redirect_to admin_users_path, notice: "Failed"
    end
  end
end
