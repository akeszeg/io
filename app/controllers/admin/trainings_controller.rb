require 'csv'

class Admin::TrainingsController < AdminController
  load_and_authorize_resource

  def index
    @trainings = Training.order('starts_at DESC')
  end

  def update
    if @training.update_attributes(params[:training])
      @command = Io::Training::Update.new(training: @training)
      @command.perform

      flash[:success] = "Saved"
      redirect_to admin_trainings_path
    else
      redirect_to admin_trainings_path, notice: 'Error'
    end
  end

  def create
    @training = Training.new(params[:training])

    if @training.save
      redirect_to admin_trainings_path
    else
      redirect_to admin_trainings_path, notice: 'Error'
    end
  end

  def schedule
    @sessions = @training.sessions.order('position ASC')
    @users = User.trainers.order('name ASC')
  end

  def export
    @sessions = @training.sessions.order('position ASC')

    respond_to do |format|
      format.csv { export_csv }
    end
  end

  private

  def export_csv
    filename = I18n.l(Time.now, :format => :short) + "-#{@training.title}.csv"
    content = CSV.generate do |c|
      c << ['Date', 'Session', 'Responsibles']
      @sessions.each do |s|
        c << [s.starts_at, s.title, s.responsibles.collect {|r| r.name}]
      end
    end

    send_data content, :filename => filename
  end
end
