module Io::User
  class Register < Io::Service
    attribute :provider, Object
    attribute :uid, Object
    attribute :info, Object

    attr_reader :user

    validate :valid_domain

    VALID_DOMAINS = %w(pitechnologies.ro pitechplus.com)

    def initialize(*)
      super

      @user = ::User.new
    end

    def perform
      @user.provider = provider
      @user.uid = uid

      if info.present?
        @user.name = info['name']
        @user.email = info['email']
      end

      validate && save
    end

    def validate
      user.valid? && self.valid?
    end

    def save
      User.transaction do
        user.save!
      end
    end

    # ============
    # Validation
    # ============

    def valid_domain
      domain = user.email[/@(.+$)/, 1]

      errors.add(:base, :invalid_domain) unless VALID_DOMAINS.include?(domain)
    end
  end
end
