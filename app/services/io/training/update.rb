module Io::Training
  class Update < Io::Service
    attribute :training, Object

    def calendar
      @calendar ||= Calendar.new(training.starts_at)
    end

    def perform
      Training.transaction do
        training.sessions.each do |session|
          session.starts_at = calendar.next_date(session.position)
          session.save!
        end
      end

      true
    end
  end
end
