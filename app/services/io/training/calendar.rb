module Io::Training
  class Calendar

    attr_reader :starts_at

    def initialize(starts_at)
      @starts_at = starts_at
    end

    def next_date(position)
      if position <= 0
        date = day_or_next_weekday(starts_at)
      else
        date = next_weekday(next_date(position - 1))
      end

      date
    end

    private

    def weekend?(day)
      [0, 6, 7].include?(day.wday)
    end

    def next_weekday(date)
      date += 1.day
      day_or_next_weekday(date)
    end

    def day_or_next_weekday(date)
      while weekend?(date)
        date += 1.day
      end

      date
    end
  end
end
