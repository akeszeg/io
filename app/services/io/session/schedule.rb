module Io::Session
  class Schedule
    def initialize(training, current_user)
      @training = training
      @current_user = current_user
    end

    def sessions
      @sessions = @training.sessions

      if @current_user.cannot? :fiew_full_schedule, @training
        @sessions = @sessions.where("starts_at <= ?", Date.today)
      end

      @sessions.order('position asc').includes(:responsibles)
    end
  end
end
