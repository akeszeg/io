module Io::Session
  class Update < Io::Service
    attribute :responsible_ids, Array
    attribute :position, Integer
    attribute :title, String
    attribute :is_free, String
    attribute :wiki_pages, Object

    attribute :session, Object

    def initialize(*args)
      super

      @wiki_pages = [] unless @wiki_pages.is_a?(Array)
    end

    def perform
      update_object && save
    end

    def calendar
      @calendar ||= Io::Training::Calendar.new(session.training.starts_at)
    end

    private

    def update_object
      @session.position = position
      @session.title = title
      @session.is_free = is_free
      @session.wiki_pages = wiki_pages
      @session.starts_at = calendar.next_date(position)
      @session.responsible_ids = responsible_ids
    end

    def save
      @session.save
    end
  end
end
