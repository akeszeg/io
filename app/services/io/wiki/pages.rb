module Io::Wiki
  class Pages

    attr_reader :wiki

    def initialize(wiki)
      @wiki = wiki
    end

    def to_json
      pages.children.to_json
    end

    private

    def pages
      @pages = @wiki.pages

      root = Page.new
      current = root

      @pages.each do |page|
        current = root

        paths = page.path.split('/')
        paths.each_with_index do |data, index|
          id = index == paths.size - 1 ? page.path : nil
          current = current.child(data, id)
        end
      end

      root
    end
  end

  class Page
    attr_reader :data, :children, :attr

    def initialize(data = nil, path = nil)
      @data = data
      @children = []
      @attr = { id: sanitize_path(path) }
    end

    def sanitize_path(path)
      path.gsub(/\..*$/, '') unless path.nil?
    end

    def child(data, path)
      children.each do |child|
        return child if child.data == data
      end

      new_page = Page.new(data, path)
      children << new_page
      new_page
    end
  end
end
