class SessionNotifier < ActionMailer::Base
  default from: 'Trainers <trainers@pitechnologies.ro>'
  helper :application
  helper :schedule

  FALLBACK_ADDRESS = 'Trainers <trainers@pitechnologies.ro>'

  def reminder(session_id)
    @session = ::Session.find(session_id)

    mail(to: session_responsible_emails(@session))
  end

  def missing_responsibles(session_id)
    @session = ::Session.find(session_id)

    mail(to: FALLBACK_ADDRESS)
  end

  private

  def session_responsible_emails(session)
    @session.responsibles.map(&:email).join(',')
  end
end
