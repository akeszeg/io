class ChangeWikiPathToText < ActiveRecord::Migration
  def up
    change_column :sessions, :wiki_path, :text
  end

  def down
    change_column :sessions, :wiki_path, :string
  end
end
