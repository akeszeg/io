class AddIsActiveToTraining < ActiveRecord::Migration
  def change
    add_column :trainings, :is_active, :boolean, :default => true
  end
end
