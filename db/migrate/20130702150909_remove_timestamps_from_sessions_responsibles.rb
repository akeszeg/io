class RemoveTimestampsFromSessionsResponsibles < ActiveRecord::Migration
  def change
    remove_columns :sessions_responsibles, :created_at, :updated_at
  end
end
