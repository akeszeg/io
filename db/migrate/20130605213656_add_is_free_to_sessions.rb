class AddIsFreeToSessions < ActiveRecord::Migration
  def change
    add_column :sessions, :is_free, :boolean
  end
end
