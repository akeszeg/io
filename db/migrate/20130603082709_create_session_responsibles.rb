class CreateSessionResponsibles < ActiveRecord::Migration
  def change
    create_table :sessions_responsibles, id: false do |t|
      t.integer :session_id
      t.integer :user_id

      t.timestamps
    end
  end
end
