class RenameCourseIdToTrainingId < ActiveRecord::Migration
  def change
    rename_column :sessions, :course_id, :training_id
  end
end
