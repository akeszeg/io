class AddWikiPathToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :wiki_path, :string
  end
end
