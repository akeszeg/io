class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string :title
      t.integer :course_id
      t.integer :position

      t.timestamps
    end
  end
end
