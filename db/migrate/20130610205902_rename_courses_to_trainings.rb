class RenameCoursesToTrainings < ActiveRecord::Migration
  def change
    rename_table :courses, :trainings
  end
end
