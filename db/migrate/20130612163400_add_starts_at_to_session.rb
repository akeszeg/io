class AddStartsAtToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :starts_at, :date
  end
end
