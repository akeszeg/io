class RenameWikiPath < ActiveRecord::Migration
  def change
    rename_column :sessions, :wiki_path, :wiki_pages
  end
end
