class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :title
      t.date :starts_at

      t.timestamps
    end
  end
end
