source 'https://rubygems.org'

gem 'rails', '4.0.0'
gem 'coffee-rails','>= 3.2.2'

gem 'jquery-rails'
gem 'jquery-ui-rails'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

gem "gollum", '>= 2.4.11'

# backwards compatible
gem "protected_attributes"

# environment
gem "dotenv-rails"

# model attributes
gem "virtus", ">= 0.5.5"

# login
gem "omniauth", ">= 1.1.4"
gem "omniauth-google-oauth2"

# authorization
gem "cancan", ">= 1.6.8"
gem "rolify", ">= 3.2.0"

# views
gem "haml", ">= 3.1.7"
gem "simple_form", "~> 3.0.0.rc"

# markdown
gem "bluecloth"

# db
gem "mysql2", ">= 0.3.11"

# assets
gem 'sass-rails'
gem 'uglifier', '>= 1.0.3'
gem "bootstrap-sass", ">= 2.2.2.0"
gem 'momentjs-rails', "~> 2.0"
gem 'haml_coffee_assets'
gem "js-routes"

# cron
gem 'whenever', :require => false

gem 'airbrake'

group :development do
  gem "quiet_assets",          ">= 1.0.1"
  gem "better_errors",         ">= 0.3.2"
  gem "binding_of_caller",     ">= 0.6.8"
  gem "haml-rails", ">= 0.3.5"
  gem "ruby_parser", ">= 3.1.1"

  gem "capistrano"
  gem "rvm-capistrano"
  gem "thin"
end

group :development, :test do
  gem 'awesome_print',        '~> 1.1.0'
  gem "rspec-rails", ">= 2.12.2"
  gem "factory_girl_rails", ">= 4.2.0"
end

group :test do
  gem "email_spec"
  gem "capybara"
  gem "database_cleaner"
end

