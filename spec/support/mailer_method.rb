shared_examples "mailer method" do |notifier, mailer_method |
  it "should render successfully" do
    lambda { send_mail(mailer_method) }.should_not raise_error
  end

  describe "rendered without error" do
    let(:mail) { send_mail(mailer_method) }

    it 'should have a subject' do
      params = self.respond_to?(:subject_parameters) ? subject_parameters : {}

      mail.subject.should have_content(
                              I18n.t(notifier.name.underscore + '.' + mailer_method.to_s + '.subject', params))
    end

    describe "and delivered" do
      it "should be added to the delivery queue" do
        lambda { mail.deliver }.should change(ActionMailer::Base.deliveries, :size).by(1)
      end

    end
  end

  def send_mail(mailer_method)
    mailer.call(mailer_method)
  end
end
