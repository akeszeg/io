
FactoryGirl.define do
  factory :training do
    title "1st eva"
    starts_at Date.today
    is_active true
  end

  trait :with_sessions do
    after(:create) do |training, evaluator|
      3.times { training.sessions << FactoryGirl.create(:session) }
    end
  end
end
