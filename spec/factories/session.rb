FactoryGirl.define do
  factory :session do
    sequence(:title) { |n| "title#{n}" }
    sequence(:position) { |n| n }
    is_free false

    association :training, factory: :training
  end
end
