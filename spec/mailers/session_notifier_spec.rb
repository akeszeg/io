require 'spec_helper'

describe SessionNotifier do
  let(:user) { FactoryGirl.create(:user) }
  let(:session) { FactoryGirl.create(:session, starts_at: Date.tomorrow, responsibles: [user]) }

  let(:subject_parameters) { { starts_at: I18n.l(Date.tomorrow, format: :long_with_weekday_name_summary), title: session.title } }

  it_behaves_like "mailer method", SessionNotifier, :reminder do
    let(:mailer) { lambda { |method| SessionNotifier.send(method, session.id) } }
  end

  it_behaves_like "mailer method", SessionNotifier, :missing_responsibles do
    let(:mailer) { lambda { |method| SessionNotifier.send(method, session.id) } }
  end
end
