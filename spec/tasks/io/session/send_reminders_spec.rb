require 'spec_helper'

describe Io::Session::SendReminders do
  let(:send_reminders) { Io::Session::SendReminders }

  describe "#perform" do
    context "when no session is scheduled for tomorrow" do
      let(:session) { FactoryGirl.create(:session, starts_at: Date.today) }

      it "doesn't send any emails" do
        SessionNotifier.should_not_receive(:reminder)
        SessionNotifier.should_not_receive(:missing_responsibles)

        send_reminders.perform
      end
    end

    context "session for tomorrow" do
      context "without responsibles" do
        let!(:session) { FactoryGirl.create(:session, starts_at: Date.tomorrow) }

        it "sends missing_responsibles" do
          SessionNotifier.should_receive(:missing_responsibles).and_return(mock(deliver: true))
          send_reminders.perform
        end
      end

      context "with responsibles" do
        let!(:session) { FactoryGirl.create(:session, starts_at: Date.tomorrow, responsibles: [FactoryGirl.create(:user)]) }

        it "sends reminder" do
          SessionNotifier.should_receive(:reminder).and_return(mock(deliver: true))
          send_reminders.perform
        end
      end
    end
  end
end
