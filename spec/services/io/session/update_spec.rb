require 'spec_helper'

describe Io::Session::Update do
  let(:session) { FactoryGirl.create(:session) }
  let(:update) { Io::Session::Update.new(attributes) }

  let(:attributes) { {
    session: session,
    title: "title",
    position: '1',
    is_free: '0'
  } }

  describe "#perform" do
    it "should not raise error" do
      expect{ update.perform }.not_to raise_error

      session.reload

      session.title.should eq('title')
      session.position.should eq(1)
      session.is_free.should eq(false)
      session.starts_at.should_not be_nil
    end
  end
end
