require 'spec_helper'

describe Io::Training::Update do
  let(:training) { FactoryGirl.create(:training, :with_sessions) }

  let(:attributes) { {
    training: training,
    title: "titel",
    starts_at: '12/12/2013',
    is_active: '1'
  } }

  let(:update) { Io::Training::Update.new(attributes) }

  describe "#perform" do
    it "should not raise error" do
      expect{ update.perform }.not_to raise_error
    end

    it "should reset starts_at for sessions" do
      update.perform

      training.reload.sessions.each do |session|
        session.starts_at.should_not be_nil
      end
    end
  end
end
