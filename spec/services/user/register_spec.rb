require 'spec_helper'

describe Io::User::Register do
  let(:email) { "test@example.com" }
  let(:info) { {"name" => "John Smith", "email" => email} }
  let(:uid) { 1 }
  let(:provider) { "google-auth" }

  describe "perform" do
    let(:register) { Io::User::Register.new(info: info, uid: uid, provider: provider) }
    subject { lambda { register.perform } }

    it { should_not raise_error }

    context "when email domain is gmail" do
      before { register.perform }
      let(:email) { "alma@gmail.com" }

      subject { register.perform }

      it { should == false }

      describe "errors" do
        subject { register.errors[:base].first }

        it { should == I18n.t('errors.messages.invalid_domain') }
      end
    end

    context "when valid domain" do
      before { register.perform }
      let(:email) { "alma@pitechnologies.ro" }

      subject { register.perform }

      it { should == true }
    end
  end
end
