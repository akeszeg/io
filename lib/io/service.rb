class Io::Service
  # Needed to handle errors, even if we don't need the actual validations here
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  extend ActiveModel::Translation

  # Provides attributes behavior for ActiveModel objects
  # See https://github.com/solnic/virtus
  include Virtus

  # Needed by ActiveModel::Conversion
  # Indicates that this model is not persisted in the db
  def persisted?
    false
  end
end
