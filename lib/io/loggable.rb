module Io::Loggable
  def log(log)
    puts "#{self}\t#{log}" unless Rails.env.test?
  end
end
