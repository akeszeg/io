class Io::Task
  extend Io::Loggable

  def self.perform(*args)
    log "Starting Task #{self} with payload #{args.inspect}"
    self.do_perform(*args)
    log "Done performing #{self}"
  end
end
