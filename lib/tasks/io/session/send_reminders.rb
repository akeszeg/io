module Io::Session
  class SendReminders < Io::Task
    def self.do_perform
      ::Session.where(starts_at: [Date.today, Date.tomorrow]).each do |session|
        if session.responsibles.any?
          log "Sending reminder for #{session.starts_at} #{session.title}"
          SessionNotifier.reminder(session.id).deliver
        else
          log "Sending missing_responsibles for #{session.starts_at} #{session.title}"
          SessionNotifier.missing_responsibles(session.id).deliver
        end
      end
    end
  end
end
