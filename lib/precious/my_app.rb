module Precious
  class MyApp < Precious::App
    include ::Io::CurrentUser

    set :gollum_path, Rails.root.join(ENV['WIKI_PATH'])
    set :default_markup, :markdown
    set :wiki_options, { universal_toc: false }

    def current_ability
      @current_ability ||= ::Ability.new(current_user)
    end

    def can?(*args)
      current_ability.can?(*args)
    end

    def cannot?(*args)
      current_ability.cannot?(*args)
    end

    def redirect_to(path, opts)
      session["flash"] ||= ::ActionDispatch::Flash::FlashHash.new

      opts.each do |k, v|
        session["flash"][k] = v
      end
      redirect(path)
    end

    def root_url
      "/"
    end

    before do
      authenticate_user!
      session["gollum.author"] = current_user.author_info
    end

    before /\/(edit|delete|create|revert|rename).*/ do
      redirect_to root_url, notice: "You are not authorized to do that" unless can? :manage, self
    end
  end
end
